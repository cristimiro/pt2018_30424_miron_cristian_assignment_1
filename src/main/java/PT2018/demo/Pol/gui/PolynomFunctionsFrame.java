package PT2018.demo.Pol.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import PT2018.demo.Pol.model.Monom;
import PT2018.demo.Pol.model.Polynom;

public class PolynomFunctionsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JButton Addition = new JButton("Addition");

	JButton Subtraction = new JButton("Subtraction");
	JButton Multiplication = new JButton("Multiplication");
	JButton Division = new JButton("Division");
	JButton Derivation = new JButton("Derivation");
	JButton Integration = new JButton("Integration");	
	JButton AddMonomToFirstPolynomBtn = new JButton("Add Monom");
	JButton AddMonomToSecondPolynomBtn = new JButton("Add Monom");
	JButton ClearFirstPolynomBtn = new JButton("Clear Polynom");
	JButton ClearSecondPolynomBtn = new JButton("Clear Polynom");			//Creat all the buttons, textfields and labels you need
	JLabel firstPolynomLabel = new JLabel("First Polynomial");
	JLabel secondPolynomLabel = new JLabel("Second Polynomial");
	JLabel resultLabel = new JLabel("Result");
	JLabel firstPolynomDisplayedOnScreen = new JLabel();
	JLabel secondPolynomDisplayedOnScreen = new JLabel();
	JLabel xForFirstPolynom = new JLabel("X^");
	JLabel xForSecondPolynom = new JLabel("X^");
	JTextField firstPolynomInputCoeficient = new JTextField();
	JTextField firstPolynomInputPower = new JTextField();
	JTextField secondPolynomInputCoeficient = new JTextField();
	JTextField secondPolynomInputPower = new JTextField();
	JTextField resultedPolynomToDisplay = new JTextField();
	Polynom firstPolynomObj = new Polynom();
	Polynom secondPolynomObj = new Polynom();

	public PolynomFunctionsFrame() throws ClassNotFoundException, InstantiationException, 
									IllegalAccessException, UnsupportedLookAndFeelException {

		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); // helps to have a simpler interface

		setSize(630, 450); 		//size of the panel
		setTitle("Polynom");	//title
		setResizable(false);	//not allows the user to extend the window

		JPanel panel = new JPanel();	//create panel
		
		panel.setLayout(null);
		panel.add(Addition);
		panel.add(AddMonomToFirstPolynomBtn);
		panel.add(AddMonomToSecondPolynomBtn);
		panel.add(ClearFirstPolynomBtn);
		panel.add(ClearSecondPolynomBtn);
		panel.add(Subtraction);
		panel.add(Multiplication);
		panel.add(Division);
		panel.add(Derivation);
		panel.add(Integration);
		panel.add(firstPolynomLabel);					//add everithing to the panel
		panel.add(secondPolynomLabel);
		panel.add(xForFirstPolynom);
		panel.add(xForSecondPolynom);
		panel.add(firstPolynomDisplayedOnScreen);
		panel.add(secondPolynomDisplayedOnScreen);
		panel.add(resultLabel);
		panel.add(firstPolynomInputCoeficient);
		panel.add(firstPolynomInputPower);
		panel.add(secondPolynomInputCoeficient);
		panel.add(secondPolynomInputPower);
		panel.add(resultedPolynomToDisplay);
//FirstPolynomDisplayedDetails
		firstPolynomLabel.setBounds(80, 10, 170, 25);
		firstPolynomInputCoeficient.setBounds(65, 40, 75, 20);
		xForFirstPolynom.setBounds(150, 40, 20, 20);
		firstPolynomInputPower.setBounds(175, 40, 75, 20);
		AddMonomToFirstPolynomBtn.setBounds(95, 65, 120, 25);
		firstPolynomDisplayedOnScreen.setBounds(65, 92, 300, 25);
		ClearFirstPolynomBtn.setBounds(95, 120, 120, 25);
//SecondPolynomDisplayedDetails
		secondPolynomLabel.setBounds(360, 10, 190, 25);
		secondPolynomInputCoeficient.setBounds(358, 40, 75, 20);
		xForSecondPolynom.setBounds(443, 40, 20, 20);
		secondPolynomInputPower.setBounds(468, 40, 75, 20);					//set bounds to all
		AddMonomToSecondPolynomBtn.setBounds(388, 65, 120, 25);
		secondPolynomDisplayedOnScreen.setBounds(360, 92, 300, 25);
		ClearSecondPolynomBtn.setBounds(388, 120, 120, 25);
//ResultDetails
		resultLabel.setBounds(275, 160, 100, 25);
		resultedPolynomToDisplay.setBounds(175, 200, 260, 40);
//Buttons
		Addition.setBounds(20, 270, 150, 50);
		Subtraction.setBounds(20, 340, 150, 50);
		Multiplication.setBounds(230, 270, 150, 50);
		Division.setBounds(230, 340, 150, 50);
		Derivation.setBounds(440, 270, 150, 50);
		Integration.setBounds(440, 340, 150, 50);
		

		AddMonomToFirstPolynomBtn.addActionListener(new ActionListener() { //add action to button
			@Override
			public void actionPerformed(ActionEvent e) {
				setActionToAddMonomButton(firstPolynomInputCoeficient, firstPolynomInputPower, firstPolynomObj, firstPolynomDisplayedOnScreen);
			}

		});

		AddMonomToSecondPolynomBtn.addActionListener(new ActionListener() {//add action to button
			@Override
			public void actionPerformed(ActionEvent e) {
				setActionToAddMonomButton(secondPolynomInputCoeficient, secondPolynomInputPower, secondPolynomObj,
						secondPolynomDisplayedOnScreen);
			}

		});

		ClearFirstPolynomBtn.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				deletePolynoms(firstPolynomInputCoeficient, firstPolynomInputPower, firstPolynomObj, firstPolynomDisplayedOnScreen);
			}
		});
		ClearSecondPolynomBtn.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				deletePolynoms(secondPolynomInputCoeficient, secondPolynomInputPower, secondPolynomObj, secondPolynomDisplayedOnScreen);
			}
		});

		Addition.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				resultedPolynomToDisplay.setText(correct(firstPolynomObj.addPolynom(secondPolynomObj).displayPolynom()));

			}

		});
		
		Subtraction.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				resultedPolynomToDisplay.setText(correct(firstPolynomObj.subtractPolynom(secondPolynomObj).displayPolynom()));
			}
			
		});
		
		Integration.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				resultedPolynomToDisplay.setText(correct(firstPolynomObj.integratePolynom(firstPolynomObj).displayPolynom()));
			}
			
		});
		
		Derivation.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				resultedPolynomToDisplay.setText(correct(firstPolynomObj.derivePolynom(firstPolynomObj).displayPolynom()));
			}
			
		});
		
		Multiplication.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				resultedPolynomToDisplay.setText(correct(firstPolynomObj.multiplyPolynom(secondPolynomObj).displayPolynom()));
			}
			
		});
		
		Division.addActionListener(new ActionListener() {//add action to button

			@Override
			public void actionPerformed(ActionEvent e) {
				resultedPolynomToDisplay.setText(correct(firstPolynomObj.dividePolynom(secondPolynomObj).displayPolynom()));
			}
			
		});

		Addition.setForeground(Color.BLACK);
		Addition.setFont(Addition.getFont().deriveFont(15.0f));
		Subtraction.setForeground(Color.BLACK);
		Subtraction.setFont(Subtraction.getFont().deriveFont(15.0f));
		Multiplication.setForeground(Color.BLACK);
		Multiplication.setFont(Multiplication.getFont().deriveFont(15.0f));
		Division.setForeground(Color.BLACK);
		Division.setFont(Division.getFont().deriveFont(15.0f));
		Derivation.setForeground(Color.BLACK);
		Derivation.setFont(Derivation.getFont().deriveFont(15.0f));		// font and size for text
		Integration.setForeground(Color.BLACK);
		Integration.setFont(Integration.getFont().deriveFont(15.0f));
		firstPolynomLabel.setForeground(Color.BLACK);
		firstPolynomLabel.setFont(firstPolynomLabel.getFont().deriveFont(20.0f));
		secondPolynomLabel.setForeground(Color.BLACK);
		secondPolynomLabel.setFont(secondPolynomLabel.getFont().deriveFont(20.0f));
		resultLabel.setForeground(Color.BLACK);
		resultLabel.setFont(resultLabel.getFont().deriveFont(20.0f));

		this.setContentPane(panel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void setActionToAddMonomButton(JTextField polynomCoefTextField, JTextField polynomPowerTextField,
			Polynom polynomObj, JLabel polynomResultLabel) {
		String firstPol1 = polynomCoefTextField.getText();
		String firstPol2 = polynomPowerTextField.getText();
		if (firstPol1.equals("") && firstPol2.equals("")) {
			JOptionPane.showMessageDialog(this, "Error. Please add at least one value!", "Error",
					JOptionPane.WARNING_MESSAGE);
		} else {
			boolean numbersOk = true;
			Integer coeficient = getIntFromString(firstPol1);
			if (coeficient == 0) {
				JOptionPane.showMessageDialog(this, "Error. Not a valid coeficient: " + firstPol1, "Error",
						JOptionPane.WARNING_MESSAGE);
				numbersOk &= false;
			}
			Integer power = getIntFromString(firstPol2);
			if (power == null) {
				power = 0;
			} else if (power < 0) {
				JOptionPane.showMessageDialog(this, "Error. Not a valid power: " + firstPol2, "Error",
						JOptionPane.WARNING_MESSAGE);
				numbersOk &= false;
			}
			if (numbersOk) {
				Monom monom = new Monom(coeficient, power);
				polynomObj.addMonomToPolynom(monom);
				polynomResultLabel.setText(correct(polynomObj.displayPolynom()));
				polynomCoefTextField.setText("");
				polynomPowerTextField.setText("");
			}

		}
	}
	private String correct(String aux) {
		while( aux.charAt( 0 ) == '+' )
		    aux = aux.substring(1);
		return aux;
	}

	private void deletePolynoms(JTextField inputFirstPolynomial1, JTextField inputFirstPolynomial2,
			Polynom firstPolynomObj, JLabel firstPolynom) {
		inputFirstPolynomial1.setText("");
		inputFirstPolynomial2.setText("");
		firstPolynomObj.clearMonomMap();
		firstPolynom.setText("");
	}

	private Integer getIntFromString(String valueToCast) {
		Integer result = null;
		try {
			result = Integer.valueOf(valueToCast);
		} catch (NumberFormatException e) {

		}
		return result;
	}
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
											IllegalAccessException, UnsupportedLookAndFeelException {
		PolynomFunctionsFrame polynomFrame = new PolynomFunctionsFrame();
		polynomFrame.setVisible(true);

}
}
