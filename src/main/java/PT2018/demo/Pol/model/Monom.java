package PT2018.demo.Pol.model;
import java.text.DecimalFormat;

public class Monom {

//Monom Class Members, the coeficient and the power of each entered Monom
	private double coeficient;
	private int power;
	private static DecimalFormat outputFormat = new DecimalFormat("#.##"); // display only 2 numbers after "."
	
// The Constructor of class Monom
	public Monom(double coeficient, int power) {	
		this.coeficient = coeficient;
		this.power = power;
	}
// Getters and Setters for each member
	public double getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getPower() {
		return power;
	}

// The following method helps the output to be better looking
	@Override
	public String toString() {
		String result = "";
		if (this.coeficient != 0) {
			if (Math.round(this.coeficient) == this.coeficient) {
				result += (int)this.coeficient;
			}else {
				result += outputFormat.format(this.coeficient);
			}
		}
		if (this.power>=0) {
			if (this.power==0) {
				return result;
			}else if(this.power==1){
				return result + "x";
			}
			else {
				return result + "x^" + this.power;
			}
		}
		return "";
		
	}

}
