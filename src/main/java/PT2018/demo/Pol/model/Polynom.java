package PT2018.demo.Pol.model;

import java.util.*;
import java.util.Map.Entry;


public class Polynom {

	//create a treeMap where all the monoms will be saved
	private Map<Integer, Monom> monomMap = new TreeMap<Integer, Monom>(Collections.reverseOrder());
	
	//2 constructors
	private Polynom(Map<Integer, Monom> monoms) {
		this.monomMap = monoms;
	}

	public Polynom() {
	}
	
	// method which helps to clear the monomMap
	public void clearMonomMap() {
		monomMap.clear();
	}

	//	Adds a monom to the polynom. if the degree of the entered monom is already a key in the map, this code will automatically
	//add the two coeficients from the given monom and the one at that specific key.
	public void addMonomToPolynom(Monom monom) {
		Integer monomPower = monom.getPower();
		Monom existingMonom = this.monomMap.get(monomPower);
		if (existingMonom == null) {
			existingMonom = new Monom(monom.getCoeficient(), monom.getPower());
			this.monomMap.put(monomPower, existingMonom);
		} else {
			existingMonom.setCoeficient(existingMonom.getCoeficient() + monom.getCoeficient());
		}
	}
	//	This method cloens a polynom, so that when i iterate over it and i remove elements it will not change the initial values of the polynom.
	protected Polynom clonePolynom() {
		Map<Integer, Monom> exactMapCopyWithNewMonoms = new LinkedHashMap<Integer, Monom>();
		for (Entry<Integer, Monom> entry : this.monomMap.entrySet()) {
			Monom value = entry.getValue();
			exactMapCopyWithNewMonoms.put(entry.getKey(), new Monom(value.getCoeficient(), value.getPower()));
		}
		return new Polynom(exactMapCopyWithNewMonoms);
	}
	
	//	This method displays the Polynom by using foreach.
	public String displayPolynom() {
		String display = "";
		for (Monom monom : this.monomMap.values()) {
			if (monom.getCoeficient()!=0) {
				if (monom.getCoeficient()>0) {
					System.out.println("+"+monom);
					display += "+" +monom;
				}else {
					System.out.println(monom);
					display += monom;
				}
			}
		}
		return display;
	}

	// This method returns the max degree of a polynomial.
	public int maxDegree() {
		int max = 0;
		for (Monom aux : this.monomMap.values()) {
			if (aux.getPower()>max) {
				max=aux.getPower();
			}
		}
		return max;
	}

	// ADUNARE
	public Polynom addPolynom(Polynom toAdd) {
		Polynom currentPolynomClone = this.clonePolynom();
		Polynom toAddClone = toAdd.clonePolynom();					//Create a clone to both Polynoms.
		Polynom addResult = new Polynom();
		Collection<Monom> currentPolynomCloneValues = currentPolynomClone.monomMap.values();		//Create a collection with all the monoms from
		Collection<Monom> toAddPolynomeCloneValues = toAddClone.monomMap.values();					//each Polynom
		for (Iterator<Monom> i1 = currentPolynomCloneValues.iterator(); i1.hasNext();) {			// while the polynom still have values, go on.
			Monom aux1FromIt = i1.next();															//save the value in a variable, so that u do not have to use again next(). if you do, the value will change again each time when we use it and we do not want this.
			for (Iterator<Monom> i2 = toAddPolynomeCloneValues.iterator(); i2.hasNext();) {			//same as before
				Monom aux2FromIt = i2.next();
				if (aux1FromIt.getPower() == aux2FromIt.getPower()) {
					addResult.addMonomToPolynom(new Monom(aux1FromIt.getCoeficient() + aux2FromIt.getCoeficient(), aux1FromIt.getPower()));
					i1.remove();	//if you found 2 monomials with the same power, add them and then remove them from the Polynoms so that you no 
					i2.remove();	//longer take them into consideration while itterating
				}
			}
		}
		Iterator<Monom> it1 = currentPolynomCloneValues.iterator();	//create another two iterators for the resulted polynomials after removing some of the items;
		Iterator<Monom> it2 = toAddPolynomeCloneValues.iterator();
		while (it1.hasNext()) {										//add to the result all the monoms which are still in the list, because they have unique power
			Monom auxIt1 = it1.next();
			addResult.addMonomToPolynom(new Monom(auxIt1.getCoeficient(), auxIt1.getPower()));
		}
		while (it2.hasNext()) {
			Monom auxIt2 = it2.next();
			addResult.addMonomToPolynom(new Monom(auxIt2.getCoeficient(),auxIt2.getPower()));
		}
		return addResult;			//return the result
	}
	// SCADERE (for subtraction, i used the same algorithm like the one for addition)
	public Polynom subtractPolynom(Polynom toSubtract) {
		Polynom currentPolynomClone = this.clonePolynom();
		Polynom toSubtractClone = toSubtract.clonePolynom();
		Polynom subtractResult = new Polynom();
		Collection<Monom> currentPolynomCloneValues = currentPolynomClone.monomMap.values();
		Collection<Monom> toSubtractPolynomeCloneValues = toSubtractClone.monomMap.values();
		for (Iterator<Monom> i1 = currentPolynomCloneValues.iterator(); i1.hasNext();) {
			Monom aux1FromIt = i1.next();
			for (Iterator<Monom> i2 = toSubtractPolynomeCloneValues.iterator(); i2.hasNext();) {
				Monom aux2FromIt = i2.next();
				if (aux1FromIt.getPower() == aux2FromIt.getPower()) {
					subtractResult.addMonomToPolynom(new Monom(aux1FromIt.getCoeficient() - aux2FromIt.getCoeficient(), aux1FromIt.getPower()));
					i1.remove();
					i2.remove();
				}
			}
		}
		Iterator<Monom> it1 = currentPolynomCloneValues.iterator();
		Iterator<Monom> it2 = toSubtractPolynomeCloneValues.iterator();
		while (it1.hasNext()) {
			Monom auxIt1 = it1.next();
			subtractResult.addMonomToPolynom(new Monom(auxIt1.getCoeficient(), auxIt1.getPower()));
		}
		while (it2.hasNext()) {
			Monom auxIt2 = it2.next();
			subtractResult.addMonomToPolynom(new Monom((auxIt2.getCoeficient()) * (-1), auxIt2.getPower()));
		}
		return subtractResult;
	}
	// DERIVARE (for derivation, i used the same algorithm like the one for addition, but only for one polynom)
	public Polynom derivePolynom(Polynom toDerive) {
		Polynom deriveResult = new Polynom();
		Polynom currentPolynomClone = this.clonePolynom();
		Collection<Monom> currentPolynomCloneValues = currentPolynomClone.monomMap.values();
		for (Iterator<Monom> i1 = currentPolynomCloneValues.iterator(); i1.hasNext();) {
			Monom aux1FromIt = i1.next();
			if (aux1FromIt.getPower() != 0) {
				deriveResult.addMonomToPolynom(new Monom((aux1FromIt.getCoeficient() * aux1FromIt.getPower()),(aux1FromIt.getPower() - 1)));
				i1.remove();
			} else {
				i1.remove();
			}
		}
		return deriveResult;

	}
	// INTEGRARE (for integration, i used the same algorithm like the one for addition, but only for one polynom)
	public Polynom integratePolynom(Polynom toIntegrate) {
		Polynom integrateResult = new Polynom();
		Polynom currentPolynomClone = this.clonePolynom();
		Collection<Monom> currentPolynomCloneValues = currentPolynomClone.monomMap.values();
		for (Iterator<Monom> i1 = currentPolynomCloneValues.iterator(); i1.hasNext();) {
			Monom aux1FromIt = i1.next();
			integrateResult.addMonomToPolynom(new Monom((aux1FromIt.getCoeficient() / (aux1FromIt.getPower() + 1)),(aux1FromIt.getPower() + 1)));
			i1.remove();
		}
		return integrateResult;
	}
	// INMULTIRE (for multiplication, i used the same algorithm like the one for addition)
	public Polynom multiplyPolynom(Polynom toMultiply) {
		Polynom mulResult = new Polynom();
		Polynom currentPolynomClone = this.clonePolynom();
		Polynom toAddClone = toMultiply.clonePolynom();
		Collection<Monom> currentPolynomCloneValues = currentPolynomClone.monomMap.values();
		Collection<Monom> toAddPolynomeCloneValues = toAddClone.monomMap.values();
		for (Iterator<Monom> i1 = currentPolynomCloneValues.iterator(); i1.hasNext();) {
			Monom aux1FromIt = i1.next();
			for (Iterator<Monom> i2 = toAddPolynomeCloneValues.iterator(); i2.hasNext();) {
				Monom aux2FromIt = i2.next();
				mulResult.addMonomToPolynom(new Monom(aux1FromIt.getCoeficient() * aux2FromIt.getCoeficient(), aux1FromIt.getPower() + aux2FromIt.getPower()));
			}
		}
		return mulResult;
	}
	//IMPARTIRE
	public Polynom dividePolynom (Polynom toDivide) {
		Polynom divideResult = new Polynom();
		Polynom currentPolynomClone = this.clonePolynom();
		Polynom toDivideClone = toDivide.clonePolynom();								// Initialization
		Collection<Monom> currentPolynomCloneValues = currentPolynomClone.monomMap.values();
		Collection<Monom> toDividePolynomCloneValues = toDivideClone.monomMap.values();
		
		for (Iterator<Monom> it1 = currentPolynomCloneValues.iterator();it1.hasNext();){
			Monom aux1=it1.next();						//select one by one all monomials from first polynom, but only the first for the second one
			Iterator<Monom> it2 = toDividePolynomCloneValues.iterator();
			Monom aux2=it2.next();
			if(aux1.getPower()>=aux2.getPower()) {				//if the power from the first one is >= then the second one continue
				Monom r1 = new Monom ((aux1.getCoeficient()/aux2.getCoeficient()),aux1.getPower()-aux2.getPower());
				divideResult.addMonomToPolynom(r1);		//ad too the result the division of two Monoms
				for (Monom aux : toDividePolynomCloneValues) {
					currentPolynomClone.addMonomToPolynom(new Monom (r1.getCoeficient()*aux.getCoeficient()*(-1),aux.getPower()+r1.getPower()));
				}		// multiply each element from the second polynom with the result just obtained, then add it to the first polynomial for the next division
			}
		}
		return divideResult;
	}
}
