package PT2018.demo.Pol;

import org.junit.Assert;
import org.junit.Test;

import PT2018.demo.Pol.model.Monom;
import PT2018.demo.Pol.model.Polynom;

public class PolynomTest  {
	
//FIRST TEST - ADD
	@Test
	public void polynomTest_AdditionFunction_ReturnOK () {
		String expectedResult = "+3x^7+2x^5+10x^3";
		Polynom firstPolynom = new Polynom();
		Polynom secondPolynom = new Polynom();
		Monom firstMonomOfFirstPolynom = new Monom(3, 7);
		Monom firstMonomOfSecondPolynom = new Monom(10, 3);
		Monom secondMonomOfSecondPolynom = new Monom(2, 5);
		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
		Polynom result = firstPolynom.addPolynom(secondPolynom);
		Assert.assertEquals(expectedResult, result.displayPolynom());
		firstPolynom.clearMonomMap();
	}
	
//SECOND TEST - SUB	
	@Test
	public void polynomTest_SubtractionFunction_ReturnOk () {
		String expectedResult = "+3x^7-2x^5-10x^3";
		Polynom firstPolynom = new Polynom();
		Polynom secondPolynom = new Polynom();
		Monom firstMonomOfFirstPolynom = new Monom(3, 7);
		Monom firstMonomOfSecondPolynom = new Monom(10, 3);
		Monom secondMonomOfSecondPolynom = new Monom(2, 5);
		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
		Polynom result = firstPolynom.subtractPolynom(secondPolynom);
		Assert.assertEquals(expectedResult, result.displayPolynom());
	}
	
//SECOND TEST - Integration	
	@Test
	public void polynomTest_IntegrationFunction_ReturnOk () {
		String expectedResult = "+1x^6-2x^3";
		Polynom firstPolynom = new Polynom();
		Polynom secondPolynom = new Polynom();
		Monom firstMonomOfFirstPolynom = new Monom(6, 5);
		Monom firstMonomOfSecondPolynom = new Monom(6, 5);
		Monom secondMonomOfSecondPolynom = new Monom(-6, 2);
		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
		Polynom result = secondPolynom.integratePolynom(secondPolynom);
		Assert.assertEquals(expectedResult, result.displayPolynom());
	}	
	
//SECOND TEST - Derivation	
	@Test
	public void polynomTest_DerivationFunction_ReturnOk () {
		String expectedResult = "+30x^4-12x";
		Polynom firstPolynom = new Polynom();
		Polynom secondPolynom = new Polynom();
		Monom firstMonomOfFirstPolynom = new Monom(3, 2);
		Monom firstMonomOfSecondPolynom = new Monom(6, 5);
		Monom secondMonomOfSecondPolynom = new Monom(-6, 2);
		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
		Polynom result = secondPolynom.derivePolynom(secondPolynom);
		Assert.assertEquals(expectedResult, result.displayPolynom());
	}
		
	//SECOND TEST - Multiplication
	@Test
	public void polynomTest_MultiplicationFunction_ReturnOk () {
		String expectedResult = "+18x^7-18x^4";
		Polynom firstPolynom = new Polynom();
		Polynom secondPolynom = new Polynom();
		Monom firstMonomOfFirstPolynom = new Monom(3, 2);
		Monom firstMonomOfSecondPolynom = new Monom(6, 5);
		Monom secondMonomOfSecondPolynom = new Monom(-6, 2);
		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
		Polynom result = firstPolynom.multiplyPolynom(secondPolynom);
		Assert.assertEquals(expectedResult, result.displayPolynom());
	}
	
//FIRST TEST - ADD
	@Test
	public void polynomTest_DivisionFunction_ReturnOK () {
		String expectedResult = "+3x+1";
		Polynom firstPolynom = new Polynom();
		Polynom secondPolynom = new Polynom();
		Monom firstMonomOfFirstPolynom = new Monom(3, 2);
		Monom secondMonomOfFirstPolynom = new Monom(10, 1);
		Monom thirdMonomOfFirstPolynom = new Monom(5, 0);
		Monom firstMonomOfSecondPolynom = new Monom(1, 1);
		Monom secondMonomOfSecondPolynom = new Monom(3, 0);
		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
		firstPolynom.addMonomToPolynom(secondMonomOfFirstPolynom);
		firstPolynom.addMonomToPolynom(thirdMonomOfFirstPolynom);
		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
		Polynom result = firstPolynom.dividePolynom(secondPolynom);
		Assert.assertEquals(expectedResult, result.displayPolynom());
		firstPolynom.clearMonomMap();
	}
}
